package pintxo.tabernak;

import android.app.Activity;
import android.location.LocationListener;
import android.os.Bundle;
import android.os.Build;
import android.view.Window;
import android.view.WindowManager;
import android.webkit.WebView;
import android.webkit.WebChromeClient;
import android.webkit.JsResult;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.webkit.WebSettings;
import android.graphics.BitmapFactory;
import android.app.ActivityManager.TaskDescription;
import android.content.Intent;
import android.view.View;
import android.webkit.GeolocationPermissions;
import android.Manifest;
import android.content.pm.PackageManager;
import android.content.Context;
import android.webkit.JavascriptInterface;
import android.location.LocationManager;
import android.location.Location;
import android.provider.Settings;
import android.widget.Toast;

public class MainActivity extends Activity implements LocationListener {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            getWindow().addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            getWindow().setStatusBarColor(getResources().getColor(R.color.colorPrimaryDark));
        }
        setContentView(R.layout.activity_main);

        WebView wv = (WebView) findViewById(R.id.webView);
        wv.addJavascriptInterface(new jsinterface(), "Android");
        wv.setWebChromeClient(new WebChromeClient(){

            @Override
            public boolean onJsAlert(WebView view, String url, String message, final JsResult result) {
                new AlertDialog.Builder(MainActivity.this)
                .setTitle(getResources().getString(R.string.app_name))
                .setMessage(message)
                .setNeutralButton(android.R.string.ok,
                    new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {
                            dialog.dismiss();
                        }
                    })
                .show();
                result.cancel();
                return true;
            }

            /*@Override
            public void onGeolocationPermissionsShowPrompt(String origin, GeolocationPermissions.Callback callback) {
                callback.invoke(origin, true, false);
            }*/

        });
        
        WebSettings webSettings = wv.getSettings();
        webSettings.setJavaScriptEnabled(true);
        webSettings.setGeolocationEnabled(true);
        
        wv.loadUrl("file:///android_asset/pintxo-tabernak.html");
    }

    @Override
    public void onStart() {
        super.onStart();
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            TaskDescription taskDescription = new TaskDescription(getString(R.string.app_name),
                BitmapFactory.decodeResource(getResources(), R.mipmap.ic_launcher),
                getColor(R.color.colorPrimary));
                this.setTaskDescription(taskDescription);
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.about:
                Intent aai = new Intent(MainActivity.this, AboutActivity.class);
                MainActivity.this.startActivity(aai);
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    public void kokapenaBidali(String location) {
        final WebView wv = (WebView) findViewById(R.id.webView);
        final String kokapena = location;
        wv.post(new Runnable() {
            @Override
            public void run() {
                wv.loadUrl("javascript:kokapenaAndroidetik('" + kokapena + "')");
            }
        });
        //behin kokapena lortuta bilatzen jarraitzeari utzi
        LocationManager lm = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
        lm.removeUpdates(MainActivity.this);
    }

    public class jsinterface {

        @JavascriptInterface
        public void kokapenaLortu() {
            //baimenak ditugun konprobatu
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                int p = MainActivity.this.checkCallingOrSelfPermission("android.permission.ACCESS_FINE_LOCATION");
                if(p != PackageManager.PERMISSION_GRANTED) {
                    //baimenik ez badugu eskatu
                    MainActivity.this.requestPermissions(new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, 1);
                } else {
                    //baimenak baditugu, kokapen hornitzaile aktibo dagoen konprobatu
                    LocationManager lm = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
                    if (!lm.isProviderEnabled(LocationManager.GPS_PROVIDER)) {
                        //aktibo ez badago aktibatzeko galdetu
                        AlertDialog.Builder builder = new AlertDialog.Builder(MainActivity.this);
                        builder.setTitle(getResources().getString(R.string.dialogTitle));
                        builder.setMessage(getResources().getString(R.string.dialogMessage));
                        builder.setPositiveButton(android.R.string.yes,
                            new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int which) {
                                    //baietz esaten badu ezarpenetara eraman
                                    startActivityForResult(new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS), 12344321);
                                    dialog.dismiss();
                                }
                            });
                        builder.setNegativeButton(android.R.string.no,
                            new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int which) {
                                    //ezetz esaten badu ez egin ezer
                                    dialog.dismiss();
                                }
                            });
                        builder.show();
                    } else {
                        //baimenak baditugu eta kokapen hornitzailea aktibo badago, kokapena lortu
                        lm.requestLocationUpdates(LocationManager.GPS_PROVIDER, 5000, 5, MainActivity.this);
                    }
                }
            }
        }
    }

    public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults) {
        switch (requestCode) {
            case 1: {
                //baimena eman bada
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    //baimenak baditugu, kokapen hornitzaile aktibo dagoen konprobatu
                    LocationManager lm = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
                    if (!lm.isProviderEnabled(LocationManager.GPS_PROVIDER)) {
                        //aktibo ez badago aktibatzeko galdetu
                        AlertDialog.Builder builder = new AlertDialog.Builder(MainActivity.this);
                        builder.setTitle(getResources().getString(R.string.dialogTitle));
                        builder.setMessage(getResources().getString(R.string.dialogMessage));
                        builder.setPositiveButton(android.R.string.yes,
                            new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int which) {
                                    //baietz esaten badu ezarpenetara eraman
                                    startActivityForResult(new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS), 12344321);
                                    dialog.dismiss();
                                }
                            });
                        builder.setNegativeButton(android.R.string.no,
                            new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int which) {
                                    //ezetz esaten badu ez egin ezer
                                    dialog.dismiss();
                                }
                            });
                        builder.show();
                    } else {
                        //baimenak baditugu eta kokapen hornitzailea aktibo badago, kokapena lortu
                        lm.requestLocationUpdates(LocationManager.GPS_PROVIDER, 5000, 5, MainActivity.this);
                    }
                } else {
                    //baimena ez da eman
                }
                return;
            }
        }
    }

    //ezarpenetatik bueltan kokapena aktibo dagoen konprobatu
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        //kokapena aktibo badago bilatzen hasi
        LocationManager lm = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
        if (lm.isProviderEnabled(LocationManager.GPS_PROVIDER)) {
            lm.requestLocationUpdates(LocationManager.GPS_PROVIDER, 5000, 5, MainActivity.this);
        }
    }

    @Override
    public void onLocationChanged(Location location) {
        try {
            kokapenaBidali(location.getLatitude() + ", " + location.getLongitude());
        } catch (Exception e) {
            Toast.makeText(MainActivity.this, e.getMessage(), Toast.LENGTH_LONG).show();
        }
    }
 
    @Override
    public void onProviderDisabled(String provider) {

    }
 
    @Override
    public void onStatusChanged(String provider, int status, Bundle extras) {

    }
 
    @Override
    public void onProviderEnabled(String provider) {

    }

    //aplikazioa ixterakoan kokapena bilatzeari utzi
    @Override
    protected void onPause() {
        super.onPause();
        LocationManager lm = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
        lm.removeUpdates(MainActivity.this);
    }
}